package com.mail;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageHelper;
import com.mail.entiy.MstDict;
import com.mail.service.impl.MstDictService;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=MailProducerApplication.class)
public class MailProducerApplicationTests {
	
	private static Logger LOGGER = LoggerFactory.getLogger(MailProducerApplicationTests.class);
	
	@Resource(name="masterDataSource")
	private DataSource masterDataSource1;
	@Resource(name="slaveDataSource")
	private DataSource slaveDataSource;
	@Value(value="${druid.master.driver-class-name}")
	private String jdbc;
	
	@Test
	public void contextLoads() throws SQLException {
		System.out.println(jdbc);
		Connection c1 = masterDataSource1.getConnection();
		System.err.println(c1.getMetaData().getURL());
		
		Connection c2 = slaveDataSource.getConnection();
		System.err.println(c2.getMetaData().getURL());
	}
	
	@Autowired
	private MstDictService mstDictService;
	@Test
	public void findMstDict() {
		List<MstDict> list = mstDictService.findMstDict();
		for(MstDict info :list) {
			System.out.println(info.getName());
		}
	}
	
	@Test
	public void findMstDictPage() {
		PageHelper.offsetPage(1, 3);
		List<MstDict> list = mstDictService.findMstDict();
		for(MstDict info :list) {
			System.out.println(info.getName());
		}
	}
	/*@Autowired
	private RedisTemplate<String, String> redisTemplate; 
	@Test
	public void cachedRedis() {
		redisTemplate.opsForValue().set("name", "wangpeng");
		System.out.println(redisTemplate.opsForValue().get("name"));
	}*/
}
