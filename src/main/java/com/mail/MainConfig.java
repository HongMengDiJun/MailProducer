package com.mail;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * 
 * @author wp
 *@Configuration  告诉Spring Boot这是一个
 */
@EnableWebMvc //启动了spring mvc
@Configuration //让Spring Boot 项目启动时识别当前配置类
@EnableAutoConfiguration
@MapperScan({"com.mail.mapper"})
@ComponentScan(basePackages="com.mail.*") //全局扫描
public class MainConfig {

}
