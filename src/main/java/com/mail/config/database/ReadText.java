package com.mail.config.database;

public class ReadText {
	/**
	 * 首先我们要连接数据源 DataSourceConfiguration.java
	 * 
	 * 第二步我们需要创建DataSourceContextHolder.java 创建两个常量（主从标签），放到ThreadLocal局部变量中，通过这个常量标签来确定我们调用那个数据库
	 * 
	 * 第三步我们来创建MybatisConfiguration.java  创建SqlSessionFactory 这个bean，并注入数据源
	 * 
	 * 因为有两个数据源所以为了方便管理，我们把两个数据源交给AbstractRoutingDataSource这个路由类来动态管理指定具体要连接哪一个数据源
	 * 
	 * SqlSessionFactory 因为只能有一个数据源，所以我们通过AbstractRoutingDataSource来动态的指定数据源，要使用这个路由类，
	 * 		首先我们需要创建一个ReadWriteSplitRoutingDataSource.java实现AbstractRoutingDataSource这个路由类来重写一个determineCurrentLookupKey()方法，
	 * 		 这个方法里面我们通过DataSourceContextHolder.getDataBaseType()获取存储在ThreadLocal中的常量，通过这个可以识别是主还是从数据源
	 * 
	 * 		接下来我们需要在MybatisConfiguration.java中再创建一个方法roundRobinDataSourceProxy()
	 * 			返回值为AbstractRoutingDataSource 
	 * 			首先new 一个ReadWriteSplitRoutingDataSource ,我们把主从数据源放入到map中，在把map注入到ReadWriteSplitRoutingDataSource中，让它来管理这两个数据源
	 * 			其次设定一个默认的数据源，就是在默认情况SqlSessionFactory连接的是主数据源，
	 * 			到此我们初步完成了SqlSessionFactory的创建以及并把数据源注入进去，但是还有一个问题就是我们怎么强制把主数据源切换到从数据源上去呢？下面继续分析
	 * 
	 * 
	 * 			这里我们采用自定义注解，
	 * 			我们定义一个注解就叫ReadOnlyConnection
	 * 			同时给这个注解织入一个切面，在调用注解的时候，先执行这个切面，这个切面会强制切换主数据源到从数据源上，  
	 * 			
	 * 			当使用这个ReadOnlyConnection注解的时候，就能把主数据源切换到从数据源上，完成主从的切换
	 * 
	 *
	 * 			
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
}
