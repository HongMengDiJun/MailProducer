package com.mail.config.database;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;
/**
 * 
 * @author wp
 * 使用tk.mybatis 里面已经帮我写好了基础的一些方法，我们就无需在生成了
 * @param <T>
 */
public interface BaseMapper<T> extends Mapper<T>,MySqlMapper<T> {

}
