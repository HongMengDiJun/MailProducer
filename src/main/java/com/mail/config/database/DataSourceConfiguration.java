package com.mail.config.database;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;


@Configuration  //  该注解表名这是一个配置类，在Spring Boot 初始化的时候会加载进来
@EnableTransactionManagement //开启事务管理
public class DataSourceConfiguration {

	private static Logger LOGGER = LoggerFactory.getLogger(DataSourceConfiguration.class);
	
	
	/**
	 * 从yml文件中读取druid类型
	 * @value Spring Boot 的特性，从yml文件中读取druid.type的value
	 * ? 表示 读取的 ${druid.type} 的值
	 * extends DataSource 表示读取的这个值不管是什么都是继承DataSource的
	 * */
	@Value("${druid.type}")
	private Class<? extends DataSource> dataSourceType;
	
	/**创建主数据源   
	 * 1.因需在Spring Boot 启动时加载主数据源，所以要添加@Bean注解，放到 Spring 容器中去
	 * 2.因需加载yml文件中的关于主数据源的配置，所以需要如下注解@ConfigurationProperties(prefix="druid.master") 以为加载前缀为druid.master的所有配置
	 * 3.@Primary  优先加载
	 * */
	@Bean(name="masterDataSource")
	@Primary   
	@ConfigurationProperties(prefix="druid.master")
	public DataSource masterDataSource () throws SQLException{
		DataSource masterDataSource = DataSourceBuilder.create().type(dataSourceType).build();
		LOGGER.info("========MASTER: {}=========", masterDataSource);
		return masterDataSource;
		
	}
	
	
	/**创建从数据源*/
	@Bean(name="slaveDataSource") 
	@ConfigurationProperties(prefix="druid.slave")
	public DataSource slaveDataSource () throws SQLException{
		DataSource slaveDataSource = DataSourceBuilder.create().type(dataSourceType).build();
		LOGGER.info("========SLAVE: {}=========", slaveDataSource);
		return slaveDataSource;
		
	}
	
	@Bean
	public ServletRegistrationBean druidServlet() {
  	
		ServletRegistrationBean reg = new ServletRegistrationBean();
		reg.setServlet(new StatViewServlet());
//      reg.setAsyncSupported(true);
		reg.addUrlMappings("/druid/*");
		reg.addInitParameter("allow", "localhost");
		reg.addInitParameter("deny","/deny");
//      reg.addInitParameter("loginUsername", "bhz");
//      reg.addInitParameter("loginPassword", "bhz");
		LOGGER.info(" druid console manager init : {} ", reg);
		return reg;
  }

	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new WebStatFilter());
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico, /druid/*");
		LOGGER.info(" druid filter register : {} ", filterRegistrationBean);
		return filterRegistrationBean;
	}
}
