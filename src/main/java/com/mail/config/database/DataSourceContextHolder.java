package com.mail.config.database;

public class DataSourceContextHolder {
	//声明枚举类型方法
	public enum DataBaseType{
		MASTER,SLAVE
	}
	
	//为了不影响其他线程，所以选用ThreadLocal最为安全和稳妥
	/**线程局部变量*/
	private static final ThreadLocal<DataBaseType> contextHolder=new ThreadLocal<DataBaseType>();
	
	/**给局部变量设置*/
	public static void setDateBaseType(DataBaseType dateBaseType) {
		if(dateBaseType==null) {
			throw new NullPointerException();
		}
		contextHolder.set(dateBaseType);
	}
	
	public static DataBaseType getDataBaseType() {
		return contextHolder.get()==null?DataBaseType.MASTER:contextHolder.get();
	}
	/**用完清空 局部变量，这样不影响下一个线程重新设置数据源（下一个线程重新主从）*/
	public static void clearDataBaseType() {
		contextHolder.remove();
	}
}
