package com.mail.config.database;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 注意自定义注解的时候一定要新建这个类
 * 
 * 
 * 1。自定义注解必须要添加的注解，@Target
 * 	1）ElementType.METHOD 这个注解使用在方法上，
 * 	2）ElementType.TYPE 接口、类、枚举、注解
 *  3）还需要为该注解写一个aop，任何一个方法使用该注解之前先执行aop,把数据源转换为slave
 * @author wp
 *
 */
@Target({ElementType.METHOD,ElementType.TYPE})   
@Retention(RetentionPolicy.RUNTIME) //表示运行时该注解生效
public @interface ReadOnlyConnection {

}
