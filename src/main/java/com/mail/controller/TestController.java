package com.mail.controller;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
@Api(value="/testController",tags="测试数据源")
@RestController
@RequestMapping(path="/testController")
public class TestController {
	
	@Resource(name="masterDataSource")
	private DataSource masterDataSource1;
	@Resource(name="slaveDataSource")
	private DataSource slaveDataSource;
	@Value(value="${druid.master.driver-class-name}")
	private String jdbc;
	@ApiOperation(notes="",value="数据源")
	@RequestMapping(path="/contextLoads",method=RequestMethod.POST)
	public void contextLoads() throws SQLException {
		System.out.println(jdbc);
		Connection c1 = masterDataSource1.getConnection();
		System.err.println(c1.getMetaData().getURL());
		
		Connection c2 = slaveDataSource.getConnection();
		System.err.println(c2.getMetaData().getURL());
	}
}
