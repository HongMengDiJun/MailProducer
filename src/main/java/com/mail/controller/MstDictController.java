package com.mail.controller;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.mail.constant.Const;
import com.mail.entiy.MailSend;
import com.mail.entiy.MstDict;
import com.mail.eunm.MailSatus;
import com.mail.service.impl.MailSendService;
import com.mail.service.impl.MstDictService;
import com.mail.utils.KeyUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
@Api(value="/test", tags="测试接口模块")
@RestController
@RequestMapping(path="/mstDictController")
public class MstDictController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(MstDictController.class);
	@Autowired
	private MstDictService mstDictService;
	@Autowired
	private MailSendService mailSendService;
	@Autowired
	private RedisTemplate redisTemplate;
	
	/**
	 * produces指定入参的对象类型
	 * */
	@ApiOperation(value="新增邮件，并发布到队列", notes="")
	@ApiImplicitParam(name = "mailSend", value = "用户详细实体mailSend", required = true, dataType = "MailSend")
	@RequestMapping(path="/send",produces="application/json" ,method=RequestMethod.POST)
	public void send(@RequestBody(required=false) MailSend mailSend) {
		
		//String id = KeyUtil.generatorUUID();
		//填充数据库
		mailSend.setSendId(KeyUtil.generatorUUID());
		mailSend.setSendCount(0L);
		mailSend.setSendStatus(MailSatus.DRAFT.getCode());
		mailSend.setVersion(0L);
		mailSend.setUpdateBy(Const.SYS_RUNTIME);
		mailSendService.send(mailSend);
		//添加队列(redis)并更新数据库状态
		/*mailSendService.addlist(mailSend);*/
		System.out.println(1234567);
	}
	
	@ApiOperation(value="全查询", notes="")
	@RequestMapping(path="/findMstDict",method=RequestMethod.POST)
	public void findMstDict() {
		List<MstDict> list = mstDictService.findMstDict();
		for(MstDict info :list) {
			System.out.println(info.getName());
		}
	}
	@ApiOperation(value="分页查询", notes="")
	@RequestMapping(path="/findMstDictPage",method=RequestMethod.POST)
	public void findMstDictPage() {
		PageHelper.offsetPage(1, 3);
		List<MstDict> list = mstDictService.findMstDict();
		for(MstDict info :list) {
			System.out.println(info.getName());
		}
	}
	
	
	@ApiOperation(value="redis-String 添加", notes="")
	@RequestMapping(path="/optionsRedis",method=RequestMethod.POST)
	public void cacheRedis(@RequestBody(required=true)String key,@RequestBody(required=true)String val) {
		ValueOperations<String,String> value =  redisTemplate.opsForValue();
		value.set(key, val);
		
		System.out.println(value.get(key));
	}
	
	@RequestMapping(path="/queryRedis",method=RequestMethod.POST)
	public void queryRedis(@RequestBody(required=true)String key) {
		ValueOperations<String,String> value =  redisTemplate.opsForValue();
		System.out.println(value.get(key));
	}
	
}
