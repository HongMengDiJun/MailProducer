package com.mail.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mail.entiy.MstDict;
import com.mail.mapper.MstDictMapper;

@Service
public class MstDictServiceImpl implements MstDictService {
	
	@Autowired
	private MstDictMapper mstDictMapper;
	
	public List<MstDict>   findMstDict() {
		
		return mstDictMapper.selectAll();
	}
}
