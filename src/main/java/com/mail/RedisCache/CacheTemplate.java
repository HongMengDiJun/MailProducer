package com.mail.RedisCache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


@Component
public class CacheTemplate {
	@Autowired
	private  RedisTemplate redisTemplate;
	
	// start list
	public  Long rightPush(String key ,String obj) {
		ListOperations listOperations = redisTemplate.opsForList();
	    Long  result = listOperations.rightPushIfPresent(key,obj);
	    return result;
	}
	
	public  Long leftPush(String key,String obj) {
		ListOperations listOperations = redisTemplate.opsForList();
	    Long  result = listOperations.leftPushIfPresent(key,obj);
	    return result;
	}
	
	public  Long size(String key) {
		Long size = redisTemplate.opsForList().size(key);
		return size;
	}

}
