package com.mail;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Spring Boot 主入口
 * @author wp
 *
 */
@SpringBootApplication
public class MailProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailProducerApplication.class, args);
	}
}
