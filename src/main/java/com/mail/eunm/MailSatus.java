package com.mail.eunm;

public enum MailSatus {
	DRAFT("0"),//暂存/待发送
	
	SEND_IN("1"),//发送中，以进入队列
	
	NEED_OK("2"),//发送成功
	
	NEET_ERR("3");//发送失败
	
	private String code;
	
	private MailSatus(String code) {
		this.code=code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
