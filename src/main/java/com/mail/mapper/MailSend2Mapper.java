package com.mail.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mail.entiy.MailSend;
@Mapper
public interface MailSend2Mapper {
   public int deleteByPrimaryKey(String sendId);

   public  int insert(MailSend record);

   public  int insertSelective(MailSend record);

   public  MailSend selectByPrimaryKey(String sendId);

   public  int updateByPrimaryKeySelective(MailSend record);

   public int updateByPrimaryKey(MailSend record);
}