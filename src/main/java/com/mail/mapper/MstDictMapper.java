package com.mail.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mail.config.database.BaseMapper;
import com.mail.entiy.MstDict;
@Mapper
public interface MstDictMapper extends BaseMapper<MstDict>{
    
}